################################################################################################################################
train:
    * anno_bbox/ssd_anno_data.py
    * use: train model
    * solver_param:
        - "eval_type" : "detection(or classification)"
        - solver.cpp : ./src/caffe/solver.cpp
            -- diff from original caffe:
                * rename 'Solver<Dtype>::Test(const int test_net_id)' to 'Solver<Dtype>::TestClassification(const int test_net_id)'
                * add 'void Solver<Dtype>::TestDetection(const int test_net_id)' to handle 'detection' eval_type.
                * header : ./include/caffe/solver.hpp diff from original caffe:
                    -  void Test(const int test_net_id = 0);
                    +  void TestClassification(const int test_net_id = 0);
                    +  void TestDetection(const int test_net_id = 0);

    * deploy net results directly from net.detection_out = L.DetectionOutput(... ...
        - ./src/caffe/layers/detection_output_layer.cpp (new layer added by SSD, compared with original caffe)
        - ./src/caffe/layers/detection_evaluate_layer.cpp (new layer added by SSD, compared with original caffe)

################################################################################################################################
test:
    * anno_bbox/score_ssd_anno.py
        - line 347(roughly): set 'gpus' to empty to use cpu(in case of requiring test while training).

################################################################################################################################
mAP evaluation impl in source code:
    ssd_anno_data.py : solver_param:{ ... "eval_type" : "detection" ...}
        -> solver.cpp : TestDetection() : mAP <-

################################################################################################################################
classification evaluation impl in source code:
    ssd_anno_data.py : solver_param:{ ... "eval_type" : "classification" ... }
        -> solver.cpp : TestClassification() : mean_score <-
