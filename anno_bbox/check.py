import cfg
import utile
import os

os.chdir(os.path.join(cfg.ROOT_CAFFE,'anno_bbox'))

utile.xml_prepare_items(keep_items=cfg.class5e)
utile.xml_max_min_regularize()
utile.xml_max_height()
utile.get_mean_for_img()
utile.train_txt_prepare()
