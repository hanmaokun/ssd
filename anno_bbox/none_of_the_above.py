from __future__ import print_function
import fnmatch
import os
import glob

import xml.etree.ElementTree as ET

def scan_dir(root_dir):
    for Root_Dir, dirnames, filenames in os.walk(root_dir):
        for fn in fnmatch.filter(filenames, "*.xml"):
            f_name=os.path.join(Root_Dir, fn)
            tree = ET.parse(f_name)
            root = tree.getroot()
            objs = root.findall('object')
            for obj in objs:
                name = obj.find('name').text
                if name == 'none_of_the_above':
                    obj.find('name').text='QiTa_C'
            tree.write(f_name)


def main():
    ROOT = "/home/image/anno_data_2"
    scan_dir(ROOT)

if __name__ == '__main__':
    main()