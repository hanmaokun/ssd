import cfg
import utile
import os

os.chdir(os.path.join(cfg.ROOT_CAFFE,'anno_bbox'))

utile.get_mean_for_img()

#Remove extra items in xml.
utile.xml_prepare_items(keep_items=cfg.class5e)

#Prepare TXT files
utile.train_txt_prepare()


#Test the existence of .jpg files by:

list1= utile.get_file_names(mode='ALL', file_type='XML')


list1= utile.get_file_names(mode='ALL', file_type='XML')
list2= utile.get_file_names(mode='ALL')

for fn in list1:
    exist = False
    for ext in cfg.IMG_EXTENTION:
         if os.path.isfile(fn[:-3]+ext):
            if fn[:-3]+ext not in list2:
                print(fn[:-3]+ext)
            continue

