'''
Detection Pipeline:

input_image.jpg ---> fix_exif ---> detect ---> input_image_draw_frame.jpg ---> <crop?> --yes--> crop&<rotate?>

usage example:
* python anno_bbox/detect.py --model=/home/nlp/bigsur/devel/working-models/ssd896-6class-mAP92dot0/ \
    --src=/home/nlp/bigsur/devel/test/1.jpeg \
    --lm=/home/nlp/bigsur/devel/working-models/ssd896-1st-mAP94dot36/anno.prototxt --crop=True

    Will generate from input image '1.jpeg' to '1_ShouJu_MenZhen.jpg'

*python anno_bbox/detect.py --model=/home/nlp/bigsur/devel/working-models/ssd1024-2nd-mAP91dot04/ \
    --src=/home/nlp/bigsur/devel/test/1_ShouJu_MenZhen.jpg \
    --lm=/home/nlp/bigsur/devel/working-models/ssd1024-2nd-mAP91dot04/anno.prototxt  --crop=True

    Will generate cropped 4 tiny images from '1_ShouJu_MenZhen'

Returns:
* image_class:
    image classfication label, from [ShouJu_MenZhen, ShouJu_ZhuYuan, QiTa_C]

* dicts_crop
    each elements is a pair like:
        'label': xmin, ymin, width, height
            label is one of [ShouJu_MenZhen, ShouJu_ZhuYuan, \
                             RIQI_LABEL_STR, YIYUAN_LABEL_STR, JINE1_LABEL_STR, BIANHAO_LABEL_STR]


STEPS TO GENERATE TRAINING DATA FROM ORIGINAL ANNOTATION IMAGES AND XMLS:
    a. copy original data folder to a new folder, as data will be modified online
        (eg. copy .../original_data .../20170323_for_det)
    b. python anno_bbox/detect.py --src=/home/nlp/bigsur/devel/20170323_for_det/ \
                --lm=anno_bbox/anno.prototxt --crop=True
        During this step, MenZhen images will be selected and then rotated, cropped, in case needed.
    c. 3865 images left after doing above steps, after checked manually, we select 3835 images. Those 
        30 images has issues with rotation(mostly 180 rotated, due to bad YinZhang detection)
    d. then execute check.py to get data statistics and prepare files for lmdb generation.(modify keep_items
        in check.py according to training target)
    e. execute create_data.sh to generate final lmdb files.

'''

from __future__ import print_function
import numpy as np
import fnmatch
import os, sys
import random
import cv2
import cfg
import utile
import glob
import argparse
import caffe
from google.protobuf import text_format
from caffe.proto import caffe_pb2
from utile_img import fixExif, rotate_bound
import xml.etree.cElementTree as ET

# finetune to use
parser = argparse.ArgumentParser(description="For usage.")

parser.add_argument("--model", default="/home/nlp/bigsur/devel/working-models/ssd896-6class-mAP92dot0/", \
      help="Underlying SSD model directory.")
parser.add_argument("--src", default="/home/nlp/bigsur/devel/test/online/", \
      help="Original test data directory, or a single image file.")
parser.add_argument("--caffe", default="~/SSD/", \
      help="Working project directory.")
parser.add_argument("--lm", default="./anno_bbox/anno.prototxt", \
      help="labelmap file shipped with model.")
parser.add_argument("--crop", default=False, \
      help="crop detected results as sub image, choose between 'true' and 'false'.")

args = parser.parse_args()

MODEL_DIR = args.model
SRC_DIR = args.src
CAFFE_DIR = args.caffe
LABELMAP_FILE = args.lm
CROP_IMAGE = args.crop
IMG_CLASS = "QiTa_C"
DEAL_ROTATION = False
IMG_RESULT_ANNO = "_draw_frame"

# Environment variables overwrites cmdline settings
MODEL_DIR = os.getenv('MODEL_DIR', MODEL_DIR)
CAFFE_DIR = os.getenv('CAFFE_DIR', CAFFE_DIR)

YINZHANG_STR = 'YinZhang'
MENZHEN_LABEL_STR = 'ShouJu_MenZhen'
ZHUYUAN_LABEL_STR = 'ShouJu_ZhuYuan'
BIANHAO_LABEL_STR = 'BianHao'
RIQI_LABEL_STR = 'RiQi'
YIYUAN_LABEL_STR = 'YiYuan'
JINE1_LABEL_STR = 'JinE_1'
MODEL1_LABELS = ['Background', MENZHEN_LABEL_STR, ZHUYUAN_LABEL_STR, IMG_CLASS, BIANHAO_LABEL_STR]
MODEL2_LABELS = ['Background', BIANHAO_LABEL_STR, RIQI_LABEL_STR, YIYUAN_LABEL_STR, JINE1_LABEL_STR]
FRAME_STR = 'Frame'
YINZHANG_CONF_LEVEL = 0.7
MENZHEN_CONF_LEVEL = 0.4
ZHUYUAN_CONF_LEVEL = 0.4
YINZHANG_OVERLAP_LEVEL = 0.55

if not os.path.isdir(MODEL_DIR):
    print('Set a valid model directory first.')
    os._exit()

if not os.path.isdir(CAFFE_DIR):
    print('Set a valid working directory.')
    os._exit()

if not os.path.isfile(LABELMAP_FILE):
    print('Must provide a valid lablemap file.')
    os._exit()

# Make sure that caffe is on the python path:
sys.path.insert(0, 'python')

def find_model_file(model_dir):
    for Root_Dir, dirnames, filenames in os.walk(model_dir):
        for fn in fnmatch.filter(filenames, "*.caffemodel"):
            model_file_name=os.path.join(Root_Dir, fn)
            return model_file_name

model_def_file = os.path.join(MODEL_DIR, 'deploy.prototxt')
model_weights_file =  os.path.join(MODEL_DIR, find_model_file(MODEL_DIR))

img_list = []
print("src: " + SRC_DIR)
if os.path.isdir(SRC_DIR):
    img_list=utile.get_file_names(dir_name=SRC_DIR, mode='ALL')
else:
    img_list.append(SRC_DIR)

colors=[[255,0,0],[0,255,0],[0,255,255], [200,150,255],[0,255,255],[255,0,255],[255,255,255]]

ref_angles = [-180., -90., 0., 90., 180.]

# when rotation is anticlock-wise
#rotation_values = [270, 180, 90, 0, 270]
# when rotation is clock-wise
rotation_values = [90, 180, -90, 0, 90]

caffe.set_mode_gpu()
caffe.set_device(1)

# output labels by prototxt file
labelmap_file = LABELMAP_FILE
file = open(labelmap_file, 'r')
labelmap = caffe_pb2.LabelMap()
text_format.Merge(str(file.read()), labelmap)
num_annos = len(labelmap.item)
for i in xrange(0, num_annos):
    per_label = labelmap.item[i].name
    if YINZHANG_STR == per_label:
        DEAL_ROTATION = True
        break
DEAL_ROTATION = False
net = caffe.Net(model_def_file,      # defines the structure of the model
                model_weights_file,  # contains the trained weights
                caffe.TEST)     # use test mode (e.g., don't perform dropout)

# input preprocessing: 'data' is the name of the input blob == net.inputs[0]
transformer = caffe.io.Transformer({'data': net.blobs['data'].data.shape})
transformer.set_transpose('data', (2, 0, 1))
transformer.set_mean('data', np.array(cfg.ONLINE_MEAN)) # mean pixel
transformer.set_raw_scale('data', 255)  # the reference model operates on images in [0,255] range instead of [0,1]
transformer.set_channel_swap('data', (2,1,0))  # the reference model has channels in BGR order instead of RGB

# set net to batch size of 1
image_resize = cfg.SCALE
net.blobs['data'].reshape(1,3,image_resize,image_resize)

def trans_coord(x, y, angel, W, H, xmin, ymin):
    x = int(x)
    y = int(y)
    if angel == 90:
        return (H-y, x)
    if angel == 180:
        return (W-x, H-y)
    if angel == 270 or angel == -90:
        return (y, W-x)
    if angel == 0:
        return (x-xmin, y-ymin)

    return (x, y)

def fix_bndbox(sizes, objs, angel, W, H, xmin_, ymin_):
    if len(sizes) == 0:
        return
    #fix image size
    print("fix_bndbox -> angel: " + str(angel))
    if angel == -90 or angel == 90  or  angel == 270:
        sizes[0].find('width').text = str(H)
        sizes[0].find('height').text = str(W)
    else:
        sizes[0].find('width').text = str(W)
        sizes[0].find('height').text = str(H)

    #fix bouding box
    for obj in objs:
        bndbox = obj.find('bndbox')
        xmin = bndbox.find('xmin').text
        ymin = bndbox.find('ymin').text
        xmax = bndbox.find('xmax').text
        ymax = bndbox.find('ymax').text
        x, y = trans_coord(xmin, ymin, angel, W, H, xmin_ , ymin_)
        bndbox.find('xmin').text = str(x)
        bndbox.find('ymin').text = str(y)
        x, y = trans_coord(xmax, ymax, angel, W, H, xmin_, ymin_)
        bndbox.find('xmax').text = str(x)
        bndbox.find('ymax').text = str(y)

def get_labelname(labelmap, labels):
    num_labels = len(labelmap.item)
    labelnames = []
    if type(labels) is not list:
        labels = [labels]
    for label in labels:
        found = False
        for i in xrange(0, num_labels):
            if label == labelmap.item[i].label:
                found = True
                labelnames.append(labelmap.item[i].name)
                break
        assert found == True
    return labelnames

def calc_overlap(idx_yz, idx_f, xmins, ymins, xmaxs, ymaxs):
    bbox2_xmin = xmins[idx_f]
    bbox2_ymin = ymins[idx_f]
    bbox2_xmax = xmaxs[idx_f]
    bbox2_ymax = ymaxs[idx_f]
    bbox1_xmin = xmins[idx_yz]
    bbox1_ymin = ymins[idx_yz]
    bbox1_xmax = xmaxs[idx_yz]
    bbox1_ymax = ymaxs[idx_yz]

    if ((bbox2_xmin > bbox1_xmax )or (bbox2_xmax < bbox1_xmin) or
            (bbox2_ymin > bbox1_ymax )or (bbox2_ymax < bbox1_ymin)):
        overl_r =0
    else :
        intersect_xmin=(max(bbox1_xmin, bbox2_xmin))
        intersect_ymin=(max(bbox1_ymin, bbox2_ymin))
        intersect_xmax=(min(bbox1_xmax, bbox2_xmax))
        intersect_ymax=(min(bbox1_ymax, bbox2_ymax))

        intersect_size=(intersect_ymax-intersect_ymin) * (intersect_xmax-intersect_xmin)
        bbox1_size=(bbox1_ymax-bbox1_ymin) * (bbox1_xmax-bbox1_xmin)
        bbox2_size = (bbox2_ymax - bbox2_ymin) * (bbox2_xmax - bbox2_xmin)
        #overl_r=intersect_size / (bbox1_size + bbox2_size - intersect_size)
        overl_r = intersect_size / bbox1_size

    return overl_r

def filter_objs(tree_root, objs, xmin_, ymin_, width, height):
    xmax_ = xmin_ + width
    ymax_ = ymin_ + height
    for obj in objs:
        bndbox = obj.find('bndbox')
        xmin = int(bndbox.find('xmin').text)
        ymin = int(bndbox.find('ymin').text)
        xmax = int(bndbox.find('xmax').text)
        ymax = int(bndbox.find('ymax').text)

        if xmin < xmin_ or ymin < ymin_ or xmax > xmax_ or ymax > ymax_:
            tree_root.remove(obj)

def detect(img_fn):
    image = caffe.io.load_image(img_fn)
    img_dst_path = os.path.splitext(img_fn)[0] + IMG_RESULT_ANNO + '.jpg'
    xml_file_path = os.path.splitext(img_fn)[0] + '.xml'

    transformed_image = transformer.preprocess('data', image)
    net.blobs['data'].data[...] = transformed_image

    # Forward pass.
    detections = net.forward()['detection_out']

    # Parse the outputs.
    det_label = detections[0,0,:,1]
    det_conf = detections[0,0,:,2]
    det_xmin = detections[0,0,:,3]
    det_ymin = detections[0,0,:,4]
    det_xmax = detections[0,0,:,5]
    det_ymax = detections[0,0,:,6]

    crops = []
    labels = []
    angels = []

    img = cv2.imread(img_fn)

    # deal with coarse cls (frame and YinZhang)
    if DEAL_ROTATION:
        #top_indices = [i for i, conf in enumerate(det_conf) if conf >= 0.6]
        
        top_indices = []
        MZs = []
        ZYs = []
        QTs = []
        YZs = []

        for i, label in enumerate(det_label):
            if label == 1.:
                MZs.append(i)
            elif label == 2.:
                ZYs.append(i)
            elif label == 3.:
                QTs.append(i)
            elif label == 4.:
                YZs.append(i)

        MZs = [MZs[i] for i in xrange(0, len(MZs)) if det_conf[MZs[i]] > 0.4]
        #MZs = [mz for mz in MZs if det_conf[mz] > MENZHEN_CONF_LEVEL]
        if len(MZs) > 4:
            MZs = [MZs[i] for i in (0, 1, 2, 3)]

        ZYs = [ZYs[i] for i in xrange(0, len(ZYs)) if det_conf[ZYs[i]] > 0.4]
        if len(ZYs) > 4:
            ZYs = [ZYs[i] for i in (0, 1, 2, 3)]

        if len(QTs) > 4:
            QTs = [QTs[i] for i in (0, 1, 2, 3)]

        YZs = [YZs[i] for i in xrange(0, len(YZs)) if det_conf[YZs[i]] > 0.7]
        if len(YZs) > 4:
            YZs = [YZs[i] for i in (0, 1, 2, 3)]

        target_frames = []
        max_overlap_qt = YINZHANG_OVERLAP_LEVEL
        max_idx_qt = -1

        if len(YZs) == 0:
            if len(QTs) > 0:
                max_idx_qt = QTs[0]
            target_frames.append([-1, max_idx_qt])

        '''
        flag that indicating already got best confidence receips in image,
        in this case ignore the second best receip, which most probobally
        is a bad one.
        '''
        got_best_idx = False

        #for i, val in enumerate(YZs):
        mz2yz = [-1, -1, -1, -1]
        dict_yz_2_maxoverlap = {}
        while len(YZs) > 0:
            val = YZs.pop()

            if not val in dict_yz_2_maxoverlap:
                dict_yz_2_maxoverlap[val] = 0.0

            max_idx_mz = -1
            if len(MZs) > 0:
                for i_mz, val_mz in enumerate(MZs):
                    overlap = calc_overlap(val, val_mz, det_xmin, det_ymin, det_xmax, det_ymax)
                    if overlap > 0.0 and overlap >= dict_yz_2_maxoverlap[val]:
                        if mz2yz[i_mz] == -1:
                            mz2yz[i_mz] = val
                            max_idx_mz = val_mz
                            dict_yz_2_maxoverlap[val] = overlap
                        else:
                            overlap_inuse = calc_overlap(mz2yz[i_mz], val_mz, det_xmin, det_ymin, det_xmax, det_ymax)

                            # not '>=', since it could be the same YinZhang
                            if overlap > overlap_inuse:
                                prev_yz_idx = mz2yz[i_mz]
                                if not prev_yz_idx in YZs:
                                    YZs.append(prev_yz_idx)
                                mz2yz[i_mz] = val
                                max_idx_mz = val_mz
                                dict_yz_2_maxoverlap[val] = overlap

            max_overlap_zy = YINZHANG_OVERLAP_LEVEL
            max_idx_zy = -1
            if len(ZYs) > 0:
                for i_mz, val_zy in enumerate(ZYs):
                    overlap = calc_overlap(val, val_zy, det_xmin, det_ymin, det_xmax, det_ymax)
                    if overlap > max_overlap_zy:
                        #max_overlap_zy = overlap
                        max_idx_zy = val_zy
                        break

            max_overlap_qt = YINZHANG_OVERLAP_LEVEL
            max_idx_qt = -1
            if len(QTs) > 0:
                for i_mz, val_qt in enumerate(QTs):
                    overlap = calc_overlap(val, val_qt, det_xmin, det_ymin, det_xmax, det_ymax)
                    if overlap > max_overlap_qt:
                        #max_overlap_qt = overlap
                        max_idx_qt = val_qt
                        break

            best_conf_thres = 0.7
            best_idx = -1           # best idx with conf greater than best_conf_thres.
            max_conf_thres = 0.5
            max_idx = -1            # incase best idx not exists, go with the max one we have.
            if max_idx_qt != -1:
                if det_conf[max_idx_qt] >= best_conf_thres:
                    best_idx = max_idx_qt
                    best_conf_thres = det_conf[max_idx_qt] - 0.3
                elif det_conf[max_idx_qt] >= max_conf_thres:
                    max_idx = max_idx_qt
                    max_conf_thres = det_conf[max_idx_qt] - 0.3

            if max_idx_mz != -1:
                if det_conf[max_idx_mz] >= best_conf_thres:
                    best_conf_thres = det_conf[max_idx_mz]
                    best_idx = max_idx_mz
                elif det_conf[max_idx_mz] >= max_conf_thres:
                    max_idx = max_idx_mz
                    max_conf_thres = det_conf[max_idx_mz]

            if max_idx_zy != -1:
                if det_conf[max_idx_zy] >= best_conf_thres:
                    best_idx = max_idx_zy
                    best_conf_thres = det_conf[max_idx_zy]
                elif det_conf[max_idx_zy] >= max_conf_thres:
                    max_idx = max_idx_zy
                    max_conf_thres = det_conf[max_idx_zy]

            if best_idx != -1:
                '''
                if not got_best_idx:
                    target_frames = []
                    top_indices = []
                got_best_idx = True
                '''
                new_frame_found = True
                for each_frame in target_frames:
                    yz_index, frame_index = each_frame
                    if frame_index == best_idx:
                        new_frame_found = False

                if new_frame_found:
                    target_frames.append([val, best_idx])
                    top_indices.append(val)
                    top_indices.append(best_idx)

            elif max_idx != -1:
                new_frame_found = True
                for each_frame in target_frames:
                    yz_index, frame_index = each_frame
                    if frame_index == max_idx:
                        new_frame_found = False

                if new_frame_found:
                    target_frames.append([val, max_idx])
                    top_indices.append(val)
                    top_indices.append(max_idx)
    
        print("detected frames: " + str(len(target_frames)))

        img_mutable = img
        for i in xrange(0, len(target_frames)):
            yz_idx, frame_idx = target_frames[i]

            xmin_ = max(int(round(det_xmin[yz_idx] * image.shape[1])),0)
            ymin_ = max(int(round(det_ymin[yz_idx] * image.shape[0])),0)
            xmax_ = min(int(round(det_xmax[yz_idx] * image.shape[1])),img_mutable.shape[1])
            ymax_ = min(int(round(det_ymax[yz_idx] * image.shape[0])),img_mutable.shape[0])
            x_z = (xmin_ + xmax_)/2 
            y_z = (ymin_ + ymax_)/2

            xmin = max(int(round(det_xmin[frame_idx] * image.shape[1])),0)
            ymin = max(int(round(det_ymin[frame_idx] * image.shape[0])),0)
            xmax = min(int(round(det_xmax[frame_idx] * image.shape[1])),img_mutable.shape[1])
            ymax = min(int(round(det_ymax[frame_idx] * image.shape[0])),img_mutable.shape[0])
            x_f = (xmin + xmax)/2
            y_f = (ymin + ymax)/2

            angel = np.arctan2(np.array(y_f - y_z), np.array(x_z - x_f)) * 180 / np.pi

            diff_angles = [np.abs(ang - angel) for ang in ref_angles]
            min_idx = diff_angles.index(min(diff_angles))

            rot_angel = rotation_values[min_idx]

            label_name = det_label[frame_idx]
            score = det_conf[frame_idx]
            labels.append(MODEL1_LABELS[int(label_name)])
            crops.append([xmin, ymin, xmax-xmin+1, ymax-ymin+1])
            angels.append(rot_angel)

            display_txt = '%s: %.2f'%(MODEL1_LABELS[int(label_name)], score)
            color = colors[int(label_name)]
            cv2.rectangle(img_mutable, (xmin, ymin), (xmax, ymax), color, 4)
            cv2.rectangle(img_mutable, (xmin_, ymin_), (xmax_, ymax_), color, 4)

            cv2.putText(img_mutable, display_txt, (xmin, ymin-10), cv2.FONT_HERSHEY_PLAIN, 3, color, 4)
        cv2.imwrite(img_dst_path, img_mutable)

    # deal with fine cls (tiny blocks like YiYuan, SN, Cost, Date...)
    else:
        img = cv2.imread(img_fn)
        top_indices=[]
        for i, lable in enumerate(det_label):
            if lable == 2.:
                label_2=i
                break
                '''
        for i, lable in enumerate(det_label[label_2:]):
            if lable == 3.:
                label_3=i+label_2
                break
        for i, lable in enumerate(det_label[label_3:]):
            if lable == 4.:
                label_4=i+label_3
                break
                '''
        top_indices.append(0)
        #top_indices.append(label_3)
        #top_indices.append(label_4)
        top_indices.append(label_2)

        #overl_r = calc_overlap(label_2, label_2+1, det_xmin, det_ymin, det_xmax, det_ymax)
        #if (det_conf[label_2+1]>0.2) and (overl_r<0.2):
        #    top_indices.append(label_2+1)

        top_conf = det_conf[top_indices]
        top_label_indices = det_label[top_indices].tolist()
        top_labels = get_labelname(labelmap, top_label_indices)
        top_xmin = det_xmin[top_indices]
        top_ymin = det_ymin[top_indices]
        top_xmax = det_xmax[top_indices]
        top_ymax = det_ymax[top_indices]

        for i in xrange(top_conf.shape[0]):
            xmin = max(int(round(top_xmin[i] * image.shape[1])),0)
            ymin = max(int(round(top_ymin[i] * image.shape[0])),0)
            xmax = min(int(round(top_xmax[i] * image.shape[1])),img.shape[1])
            ymax = min(int(round(top_ymax[i] * image.shape[0])),img.shape[0])
            score = top_conf[i]
            label = int(top_label_indices[i])
            label_name = top_labels[i]
            # record crop info
            if label_name in [BIANHAO_LABEL_STR, RIQI_LABEL_STR, YIYUAN_LABEL_STR, JINE1_LABEL_STR]:
                crops.append([xmin, ymin, xmax-xmin+1, ymax-ymin+1])
                labels.append(label_name)
                angels.append(0)

            display_txt = '%s: %.2f'%(label_name, score)
            color = colors[label]
            cv2.rectangle(img, (xmin, ymin), (xmax, ymax), color, 4)
            cv2.putText(img, display_txt, (xmin, ymin-10), cv2.FONT_HERSHEY_PLAIN, 3, color, 4)
        cv2.imwrite(img_dst_path,img)

    if CROP_IMAGE and DEAL_ROTATION:
        img = cv2.imread(img_fn)
        img_width = img.shape[1]
        img_height = img.shape[0]

        # xml load
        tree = ET.ElementTree(file=xml_file_path)
        tree_root = tree.getroot()
        sizes = tree_root.findall('size')
        objs = tree_root.findall('object')
        paths = tree_root.findall('path')

        #for i in xrange(0, len(crops)):
        if len(crops) > 0:
            i = 0
            xmin, ymin, width, height = crops[i]

            filter_objs(tree_root, objs, xmin, ymin, width, height)
            objs = tree_root.findall('object')

            label = labels[i]
            print(label + ':' + str(xmin) + '-' + str(xmax) + '-' + str(width) + '-' + str(height))
            img_cropped = img[ymin:ymin+height, xmin:xmin + width]
            img_fn_save_name = os.path.splitext(img_fn)[0] + '.jpg'
            fix_bndbox(sizes, objs, 0, width, height, xmin, ymin)

            if angels[i] != 0:
                print("cropping " + label)
                rot_angel = angels[i]

                if(rot_angel != 0):
                    #print(cropped_img_file_name + ' ' + str(rot_angel))
                    # rotate image
                    img_cropped = rotate_bound(img_cropped, rot_angel)
                    fix_bndbox(sizes, objs, rot_angel, width, height, xmin, ymin)

            if labels[i] == 'ShouJu_MenZhen':
                os.remove(img_fn)
                cv2.imwrite(img_fn_save_name, img_cropped)
                tree.write(xml_file_path)
            else:
                os.remove(img_fn)
                os.remove(xml_file_path)
        else:
            #img_fn_save_name = os.path.splitext(img_fn)[0] + '.jpg'
            #os.rename(img_fn, img_fn_save_name)
            os.remove(img_fn)
            os.remove(xml_file_path)

    #else:
        #os.remove(img_fn)
        #os.remove(xml_file_path)
        #img_fn_save_name = os.path.splitext(img_fn)[0] + '.jpg'
        #os.rename(img_fn, img_fn_save_name)

    return labels, crops, angels

#TEST
im_index = 0
for fn in img_list:
    im_index += 1
    print('Processing the '+ str(im_index)+'th file:' + fn+'...\n')
    #fixExif(fn)
    labels, crops, angels = detect(fn)
    print(labels)
    print(crops)
    print(angels)