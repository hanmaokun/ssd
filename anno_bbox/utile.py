from __future__ import print_function
import fnmatch
import os,shutil
import random
import cfg
import cv2
import statistics
import xml.etree.ElementTree as ET

def get_file_names(dir_name=cfg.ROOT_DATA_DIR,mode='TEST',ratio=10,file_type='IMG'):
    '''
    Generating list of file nams as test list, train list or both.

    mode = 'TEST', we generate test list as a list of file names.
    mode = 'TRAIN', we generate train list as a list of file name.
    mode = 'ALL', we generate all files of *.jpg and *.JPG as a list.
    mode = 'GET_TEST_FILES', we copy the image files for test to a 'TEST' folder.

    mode = 'TXT', we prepare our files 'test.txt', 'trainval.txt', 'test_name_size.txt'.

    ratio = 10 (20 allowed only), we take 10% (or 20%) of all files as test and so on.

    file_type='IMG' get .jpg , .JPG files
    file_type='XML' get .xml files
    '''

    #pwd = os.path.join(cfg.ROOT_CAFFE,'anno_bbox')
    #os.chdir(pwd)

    IMG_extention=cfg.IMG_EXTENTION
    R_lenth = len(dir_name) + 1
    if dir_name[-1] == '/':
        R_lenth = R_lenth - 1
    Test_folder = os.path.join(dir_name, 'TEST')
    if not os.path.isdir(Test_folder):
        os.mkdir(Test_folder)

    print('Rootdir:'+dir_name+'\n')
    mode=mode.upper()
    file_type=file_type.upper()
    if file_type == 'XML':
        ext = ['xml']
    elif file_type == 'IMG':
        ext = IMG_extention
    random.seed(cfg.SEED)
    if dir_name == None:
        return

    if mode not in ['TEST','TRAIN','ALL','TXT','GET_TEST_FILES']:
        print('The mode %s is NOT allowed!\n', mode)
        return

    #preparing txt mode
    if mode == 'TXT':
        ext = IMG_extention
        f_train = open('trainval.txt', 'w')
        f_test = open('test.txt', 'w')
        f_test_size = open('test_name_size.txt', 'w')
        minW = 2000
        minH = 2000
        maxW = 0
        maxH = 0
        random.seed(cfg.SEED)
    else:
        test_list = []
        train_list = []
        all_list=[]
    for root, dirnames, filenames in os.walk(dir_name):
        for extension in ext:
            for fn in fnmatch.filter(filenames, "*." + extension):
                RDM = random.randint(1, 10)
                if ratio == 10:
                    r_range= [7]
                elif ratio == 20:
                    r_range =[5,7]
                else:
                    print('ONLT ratio =10% or 20% is allowed!\n')
                    return
                if mode == 'TEST':
                    if RDM in r_range:
                       test_list.append(os.path.join(root, fn))

                elif mode == 'TRAIN':
                    if RDM not in r_range:
                        train_list.append(os.path.join(root, fn))
                elif mode == 'ALL':
                    all_list.append(os.path.join(root, fn))

                elif mode == 'GET_TEST_FILES':
                    if RDM in r_range:
                        file = os.path.join(root, fn)
                        img = cv2.imread(file)
                        tar_name = os.path.join(Test_folder,os.path.join(root[R_lenth:], fn))
                        dir_name = os.path.join(Test_folder,root[R_lenth:])
                        if not os.path.isdir(dir_name):
                            os.mkdir(dir_name)

                        cv2.imwrite(tar_name,img)


                #Writing txt files
                else:
                    img = cv2.imread(os.path.join(root, fn))
                    width = img.shape[0]
                    height = img.shape[1]
                    RDM = random.randint(1, 10)
                    if RDM in r_range:
                        f_test_size.write(fn + ' ' + str(width) + ' ' + str(height) + '\n')
                        f_test.write(os.path.join(root[R_lenth:], fn) + ' ' + os.path.join(root[R_lenth:],
                                                                                           fn[:-len(extension)] + "xml") + '\n')
                    else:
                        f_train.write(os.path.join(root[R_lenth:], fn) + ' ' + os.path.join(root[R_lenth:],
                                                                                            fn[:-len(extension)] + "xml") + '\n')

                    if height == 0 or width == 0:
                        print(fn)
                        continue

                    if width < minW:
                        minW = width
                    elif width > maxW:
                        maxW = width

                    if height < minH:
                        minH = height
                    elif height > maxH:
                        maxH = height
    if mode == 'TEST':
        return test_list
    elif mode == 'TRAIN':
        return train_list
    elif mode == 'ALL':
        return all_list
    elif mode == 'TXT':
        print("width from: " + str(minW) + " to " + str(maxW) + ", height from " + str(minH) + " to " + str(maxH))
        f_train.close()
        f_test.close()
        f_test_size.close()
        return

def xml_prepare_items(keep_items):
    '''
    :param keep_items: we remove all items in .xml files except those in keep_items

        class3=['RiQi','BianHao']
        class5=['ShouJu_MenZhen','ShouJu_ZhuYuan','QiTa_C','YinZhang']
        class7=['ShouJu_MenZhen','ShouJu_ZhuYuan','QiTa_C','YinZhang','RiQi','BianHao']

        We also prepare 'sizes' in xml for training SSD.

    :return:  clear xml files.
    '''

    if cfg.NUM_CLASSES != len(keep_items)+1:
        print('Warning: Num of classes: '+ str(cfg.NUM_CLASSES) + 'does NOT match with remaining itmes:\n')
        print(keep_items)
        print(len(keep_items))

    f_list=get_file_names(mode='ALL',file_type='XML')
    for f_name in f_list:
        valid_anno_obj_ctr = 0
        if not os.path.isfile(f_name):
            print('ERROR! (%s) is not a file.\n',f_name)
            continue
        try:
            tree = ET.parse(f_name)
            root = tree.getroot()
            objs = root.findall('object')
            for ext in cfg.IMG_EXTENTION:
                if os.path.isfile(f_name[:-3]+ext):
                    im_file=f_name[:-3]+ext
                    img = cv2.imread(im_file)
                    continue
            for obj in objs:
                name = obj.find('name').text
                if name not in keep_items:
                    root.remove(obj)
                else:
                    valid_anno_obj_ctr += 1
            sizes = root.findall('size')
            for ob in sizes:
                ob.find('width').text = str(img.shape[1])
                ob.find('height').text = str(img.shape[0])
                ob.find('depth').text= str(img.shape[2])
            tree.write(f_name)
            '''
            if valid_anno_obj_ctr/cfg.NUM_CLASSES < 0.6:
                jpg_file_name = f_name[:-4] + '.jpg'
                print(jpg_file_name)
                os.remove(jpg_file_name)
                os.remove(f_name)
            '''
        except Exception as error:
            print(str(error)+'\n')
    return

def train_txt_prepare():
    get_file_names(mode='TXT')


def get_mean_for_img():
    f_list = get_file_names(mode='ALL')
    MEAN = 0
    num_img = 0
    file = open('mean.txt', 'w')
    im_mean_r = []
    im_mean_g = []
    im_mean_b = []
    for f_img in f_list:
        im = cv2.imread(f_img)
        im = cv2.resize(im, (128, 128))
        im_mean = im.mean(1).mean(0)
        im_mean_r.append(im_mean[0])
        im_mean_g.append(im_mean[1])
        im_mean_b.append(im_mean[2])
        MEAN += im_mean
        num_img += 1
        file.write(str(im_mean) + '\n')

    print("variance: " + ' ' +str(statistics.pvariance(im_mean_r)) + ' ' \
        + str(statistics.pvariance(im_mean_g)) + ' '\
        + str(statistics.pvariance(im_mean_b)))

    im_mean_all = MEAN / num_img
    file.write(str(num_img) + ' ' + str(MEAN) + '\n' + 'MEAN: ' + str(im_mean_all))
    print(str(num_img) + ' ' + str(MEAN) + ': ' + str(im_mean_all))
    file.close()

def get_img_size():
    f_list = get_file_names(mode='ALL')
    H_all = 0
    W_all = 0
    NUM = 0
    for f_img in f_list:
        print(f_img)
        img = cv2.imread(f_img)
        width = img.shape[1]
        height = img.shape[0]
        W_all += width
        H_all += height
        NUM += 1

    mean_h = round(H_all /NUM)
    mean_w = round(W_all /NUM)
    print('mean_hight: ' + str(mean_h)+'\n')
    print('mean_width: '+ str(mean_w))

def xml_max_min_regularize():
    f_list = get_file_names(mode='ALL', file_type='XML')
    for f_xml in f_list:
        tree = ET.ElementTree(file=f_xml)
        tree_root = tree.getroot()
        sizes = tree_root.findall('size')
        objs = tree_root.findall('object')
        W = int(sizes[0].find('width').text)
        H = int(sizes[0].find('height').text)

        
        for obj in objs:
            bndbox = obj.find('bndbox')
            xmin = int(bndbox.find('xmin').text)
            ymin = int(bndbox.find('ymin').text)
            xmax = int(bndbox.find('xmax').text)
            ymax = int(bndbox.find('ymax').text)

            if xmin > xmax:
                tem = xmin
                xmin = xmax
                xmax = tem

            if ymin > ymax:
                tem = ymin
                ymin = ymax
                ymax = tem
            
            bndbox.find('xmin').text = str(max(0, xmin))
            bndbox.find('ymin').text = str(max(0, ymin))
            bndbox.find('xmax').text = str(min(xmax, W))
            bndbox.find('ymax').text = str(min(ymax, H))

        tree.write(f_xml)
        
def get_TEST_files(orig_dir = cfg.ROOT_DATA_DIR):

    tar_dir = os.path.join(orig_dir,'TEST')
    if not os.path.isdir(tar_dir):
        os.mkdir(tar_dir)
    test_txt_file = os.path.join(cfg.ROOT_CAFFE,'anno_bbox/test.txt')
    f_txt = open(test_txt_file)
    for line in f_txt.readlines():
        img_f = line.split(' ')[0]
        shutil.copy(os.path.join('/media/image/843ea63d-51c0-429f-ab4b-11b652e1c260/data/anno_data_regular/',img_f), os.path.join(tar_dir,img_f))
        #if os.path.isfile(os.path.join(tar_dir,img_f)): print "sucess"
        #you need make MenZhen file in TEST firstly



def xml_to_2items(keep_items):
    '''
    :param keep_items: we remove all items in .xml files except those in keep_items

        class3=['RiQi','BianHao']
        class5=['ShouJu_MenZhen','ShouJu_ZhuYuan','QiTa_C','YinZhang']
        class7=['ShouJu_MenZhen','ShouJu_ZhuYuan','QiTa_C','YinZhang','RiQi','BianHao']

        We also prepare 'sizes' in xml for training SSD.

    :return:  clear xml files.
    '''

    if cfg.NUM_CLASSES != len(keep_items)+1:
        print('Warning: Num of classes: '+ str(cfg.NUM_CLASSES) + 'does NOT match with remaining itmes:\n')
        print(keep_items)
        print(len(keep_items))

    f_list=get_file_names(mode='ALL',file_type='XML')
    for f_name in f_list:
        if not os.path.isfile(f_name):
            print('ERROR! (%s) is not a file.\n',f_name)
            continue
        try:
            tree = ET.parse(f_name)
            root = tree.getroot()
            objs = root.findall('object')
            for ext in cfg.IMG_EXTENTION:
                if os.path.isfile(f_name[:-3]+ext):
                    im_file=f_name[:-3]+ext
                    img = cv2.imread(im_file)
                    continue
            for obj in objs:
                name = obj.find('name').text
                if name not in keep_items:
                    root.remove(obj)
                else:
                    obj.find('name').text="Text"
            sizes = root.findall('size')
            for ob in sizes:
                ob.find('width').text = str(img.shape[1])
                ob.find('height').text = str(img.shape[0])
                ob.find('depth').text= str(img.shape[2])
            tree.write(f_name)
        except Exception as error:
            print(str(error)+'\n')
    return

def xml_max_height():
    f_list = get_file_names(mode='ALL', file_type='XML')
    max_height=0
    min_height=5
    for f_xml in f_list:
        tree = ET.ElementTree(file=f_xml)
        tree_root = tree.getroot()
        sizes = tree_root.findall('size')
        objs = tree_root.findall('object')
        W = int(sizes[0].find('width').text)
        H = int(sizes[0].find('height').text)
        for obj in objs:
            bndbox = obj.find('bndbox')
            xmin = int(bndbox.find('xmin').text)
            ymin = int(bndbox.find('ymin').text)
            xmax = int(bndbox.find('xmax').text)
            ymax = int(bndbox.find('ymax').text)

            if (ymax-ymin+0.0)/H>max_height:
                max_height=(ymax-ymin+0.0)/H
            if (ymax-ymin+0.0)/H<min_height:
                min_height=(ymax-ymin+0.0)/H    
        if max_height*960 > 190:
            print(f_xml)
            break           
        #print(str(max_height)+'\n')
    print(str(max_height*960)+'\n')
    print(str(min_height*960)+'\n')
