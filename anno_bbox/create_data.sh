#!/bin/sh

printenv | grep CAFFE_DIR
printenv | grep DATA_DIR

root_dir="$CAFFE_DIR"

cd $root_dir

redo=1
data_root_dir="$DATA_DIR"
data_processing_name="anno_bbox"
dataset_name="anno_data"
mapfile="$root_dir/$data_processing_name/5eclass.prototxt"
anno_type="detection"
db="lmdb"
min_dim=0
max_dim=0
width=0
height=0

extra_cmd="--encode-type=jpg --encoded"
if [ $redo ]
then
  extra_cmd="$extra_cmd --redo"
fi
for subset in test trainval
do
  python $root_dir/scripts/create_annoset.py --anno-type=$anno_type  --label-map-file=$mapfile --min-dim=$min_dim --max-dim=$max_dim --resize-width=$width  --resize-height=$height --check-label $extra_cmd $data_root_dir   $root_dir/$data_processing_name/$subset.txt    $data_root_dir/$db/$dataset_name"_"$subset"_"$db examples/$dataset_name
done
