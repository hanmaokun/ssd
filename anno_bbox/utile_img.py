import os, sys
import exifread
import cv2
import numpy as np

def rotate_bound(image, angle):
    # grab the dimensions of the image and then determine the
    # center
    (h, w) = image.shape[:2]
    print(h, w)
    (cX, cY) = (w // 2, h // 2)

    # grab the rotation matrix (applying the negative of the
    # angle to rotate clockwise), then grab the sine and cosine
    # (i.e., the rotation components of the matrix)
    M = cv2.getRotationMatrix2D((cX, cY), -angle, 1.0)
    cos = np.abs(M[0, 0])
    sin = np.abs(M[0, 1])

    # compute the new bounding dimensions of the image
    nW = int((h * sin) + (w * cos))
    nH = int((h * cos) + (w * sin))

    # adjust the rotation matrix to take into account translation
    M[0, 2] += (nW / 2) - cX
    M[1, 2] += (nH / 2) - cY

    # perform the actual rotation and return the image
    return cv2.warpAffine(image, M, (nW, nH))

# fix image exif information
def fixExif(filename):
    FIELD = 'EXIF DateTimeOriginal'
    fd = open(filename, 'rb')
    tags = exifread.process_file(fd)

    for FIELD in tags:
        if FIELD == 'Image Orientation':
            oritented = str(tags[FIELD])
            if 'Rotated' in oritented:
                orient_str = oritented.strip().split(' ')
                orient_angel = int(orient_str[1])
                print("Fixing exif for file: " + filename + ' ' + str(orient_angel))

                img_data = cv2.imread(filename)
                img_data = rotate_bound(img_data, orient_angel)
                cv2.imwrite(filename, img_data)
            else:
        		print("Valid exif info exists already.")
        else:
    		print("Valid exif info exists already.")

    if len(tags) == 0:
    	print("Valid exif info exists already.")

    fd.close()