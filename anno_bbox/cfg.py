# MUST be imported firstly
import sys
import os
import sys

'''
The following are always as default
'''
NUM_CLASSES_NORMAL_E = 6
NUM_CLASSES_NORMAL = 5
NUM_CLASSES_SMALL = 3
BATCH_SIZE_SMALL = 4

MENZHEN_MEAN = [155.46101864, 164.97204437, 167.63650585]
ONLINE_MEAN_E = [154.8256469,   164.95300858,  169.35045967]
ONLINE_MEAN_SSD_DIRTY=[144.79016982,  153.26694828,  158.62139233]
ONLINE_MEAN = [144.24722056,  152.70497438,  157.76501779]
OFFLINE_MEAN=[215,224,224]
ALL_MEAN=[178, 187, 189]
GPU_IDs=[0,1]

EXPAND_RATIO = 6 #data expansion ratio

class3=['RiQi','BianHao']
class5=['ShouJu_MenZhen','ShouJu_ZhuYuan','QiTa_C','YinZhang']
class5e=['YinZhang', 'XiangMuXiJie_a']
class6=['ShouJu_MenZhen','ShouJu_ZhuYuan','QiTa_C','YinZhang','BianHao']
class7=['ShouJu_MenZhen','ShouJu_ZhuYuan','QiTa_C','YinZhang','RiQi','BianHao']
class_JinE2to9 = ['JinE_2', 'JinE_3', 'JinE_4', 'JinE_5', 'JinE_6', 'JinE_7', 'JinE_8', 'JinE_9', 'JinE_10', 'JinE_11']
TEXT_STRS_4 = ['BianHao','YiYuan','RiQi','JinE_1']
#TEXT_STRS_6 = ['BianHao','YiYuan','RiQi','JinE_1','ShouJu_MenZhen','ShouJu_ZhuYuan']
TEXT_6 = ['BianHao','YiYuan','RiQi','JinE_1','XiangMu_1', 'XiangMu_2']
TEXT_XIANGMU = ['XiangMu_1', 'XiangMu_2']

DEF_BATCH_SIZE=32
DEF_TEST_BATCH_SIZE300 = 8
DEF_TEST_BATCH_SIZE484 = 4
DEF_TEST_BATCH_SIZE896 = 2

SEED=100

'''
Frequently changed.

zhu_yuan: 1443x858
Men_zhen: 1481x955
'''


NUM_CLASSES=6
SCALE=896
MIN_DIM=896
BATCH_SIZE=DEF_TEST_BATCH_SIZE896
NUM_TEST=436
LR_RATIO = 10

'''
Change when initialize the project
'''

ROOT_DATA_DIR = os.path.join(os.environ['HOME'],'anno_data_2')
ROOT_DATA_DIR = os.getenv('DATA_DIR', ROOT_DATA_DIR)

ROOT_CAFFE = os.path.join(os.environ['HOME'],'devel/SSD')
ROOT_CAFFE = os.getenv('CAFFE_DIR', ROOT_CAFFE)

IMG_EXTENTION= ['jpg','JPG','png','PNG','jpeg']

def init():
    sys.path.insert(0, "./caffe/python")
    sys.path.insert(0, "./anno_bbox")

init()
