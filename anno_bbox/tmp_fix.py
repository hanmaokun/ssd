from __future__ import print_function
import numpy as np
import fnmatch
import os, sys
import random
import cv2
import cfg
import utile
import glob
import argparse
import caffe
from google.protobuf import text_format
from caffe.proto import caffe_pb2
from utile_img import fixExif, rotate_bound
import xml.etree.cElementTree as ET

# finetune to use
parser = argparse.ArgumentParser(description="For usage.")

parser.add_argument("--src", default="/home/nlp/bigsur/devel/20170323_det")
parser.add_argument("--ref", default="/home/nlp/bigsur/devel/20170323_for_det")

args = parser.parse_args()

SRC_DIR=args.src
REF_DIR=args.ref

for root, dir_names, file_names in os.walk(SRC_DIR):
    for file_name in fnmatch.filter(file_names, '*.jpg'):
        jpg_file_path = os.path.join(root, file_name)
        ref_jpg_file_path = jpg_file_path.replace("20170323_det", "20170323_for_det")

        xml_file_path = os.path.splitext(jpg_file_path)[0] + '.xml'
        #print(xml_file_path)
        #os.remove(jpg_file_path)
        #os.remove()
        if not os.path.exists(ref_jpg_file_path):
        	print(jpg_file_path)
        	os.remove(jpg_file_path)
        	os.remove(xml_file_path)
