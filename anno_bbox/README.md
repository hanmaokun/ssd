Environment variables preparation:

copy the file 'SSD/env.ref' to 'SSD/env.sh' with the following steps

* set 'DATA_DIR' to the location of your 'xxx/anno_data_2'

* set 'CAFFE_DIR' to where your repo is cloned.

* source ./env.sh

Run:
1, run function 'xml_prepare_items()' in 'utile.py' as in 'demo.py' to prepare XML files

2, run "train_txt_prepare()"  in 'utile.py' as in 'demo.py' to prepare files "test.txt" and set 'NUM_TEST' in 'anno_bbox.cfg' with right ".

3, run creat_data.sh for lmdb files

4, run python anno_bbox/ssd_anno_data.py (revise it when needed)

What to do next?

1, write pytdhon script to verify our model on test set.

2, get cropped parts (and .xml files) for further detection.

3, use SSD for the new data with SCALE= 900x1500 (HxW)