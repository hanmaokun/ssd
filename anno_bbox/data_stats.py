from __future__ import print_function
import fnmatch
import os
import cfg
import glob

import xml.etree.ElementTree as ET

def scan_dir(root_dir):
    label_dict = {'ShouJu_MenZhen':0,'ShouJu_ZhuYuan':0,'QiTa_C':0,'YinZhang':0,'RiQi':0,'BianHao':0, 'total':0}
    for Root_Dir, dirnames, filenames in os.walk(root_dir):
        for fn in fnmatch.filter(filenames, "*.xml"):
            f_name=os.path.join(Root_Dir, fn)
            tree = ET.parse(f_name)
            root = tree.getroot()
            objs = root.findall('object')
            label_dict['total'] += 1
            for obj in objs:
                name = obj.find('name').text
                if name in ['ShouJu_MenZhen','ShouJu_ZhuYuan','QiTa_C','YinZhang','RiQi','BianHao']:
                    label_dict[name] += 1

    return label_dict

def main():
    ROOT_DATA_DIR = '~/anno_data_2/'
    ROOT_DATA_DIR = os.getenv('DATA_DIR', ROOT_DATA_DIR)
    if not os.path.exists(ROOT_DATA_DIR):
        print("please set a valid DATA_DIR env variable.")

    label_stats = scan_dir(ROOT_DATA_DIR)
    print("MenZhen :" + str(label_stats['ShouJu_MenZhen']))
    print("ZhuYuan :" + str(label_stats['ShouJu_ZhuYuan']))
    print("QiTa    :" + str(label_stats['QiTa_C']))
    print("YinZhang:" + str(label_stats['YinZhang']))
    print("Total:" + str(label_stats['total']))

if __name__ == '__main__':
    main()